import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import InfiniteScroll from 'vue-infinite-scroll'
import VueResource from 'vue-resource';
import { PulseLoader } from 'vue-spinner/dist/vue-spinner.min.js'

Vue.config.productionTip = false

Vue.use(InfiniteScroll);
Vue.use(VueResource);
Vue.use(require('vue-moment'));

new Vue({
  render: h => h(App),
  components: {
    PulseLoader
  }
}).$mount('#app')
