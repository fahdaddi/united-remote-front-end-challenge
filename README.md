
# united remote front end challenge

The task is to implement a small webapp that will list the most starred Github repos that were created in the last 30 days.

# app
In this project i use vue Js with vuetify because of its huge support from the open source community (including me :D). Also i used vue-infinite-scroll package that i already used in my current job, it's a stable version for auto scrolling and it provides the required features for this project. Moreover moment package helped me to deal with date to calculate the number of days since the last commit. Finally I choose Pulse Loader package to animate the platform in order to explain to the user that the navigator is fetching data from the API.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Live demo
See [Result](https://united-remote.herokuapp.com/).
